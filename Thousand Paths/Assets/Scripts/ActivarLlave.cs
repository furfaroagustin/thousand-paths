using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarLlave : MonoBehaviour
{
    public bool activarLlave;
    public GameObject llave;

    void Start()
    {
        activarLlave = false;
        llave.SetActive(false);
    }
    void Update()
    {
        if (activarLlave == true)
        {
            Interlude.InterludeManager.CanFindKey();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            activarLlave = true;
            llave.SetActive(true);
        }
        
    }
}

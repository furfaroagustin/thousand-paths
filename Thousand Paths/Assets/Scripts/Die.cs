﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Die : MonoBehaviour
{
	public GameObject panel;
	public int maxHealth = 100;
	public int currentHealth;
	public HealtBar healthBar;
	protected AudioSource audioSource;
	public AudioClip trampa;
	public Transform spawnPoint;
	public AudioClip risa;
	void Start()
	{
		panel.SetActive(false);
		audioSource = GetComponent<AudioSource>();
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
	}

	
	void Update()
	{


	}
	private void OnCollisionEnter(Collision colision)
	{
		if (colision.gameObject.CompareTag("Enemigo"))
		{
			
			RestaBarra(10);
			audioSource.PlayOneShot(trampa);
		}
		if (colision.gameObject.CompareTag("Objeto"))
		{
			
			RestaBarra(20);

		}

		if (currentHealth == 0)
		{
			//transform.position = spawnPoint.position;
			//Destruccion();
			Invoke("Fin", 3);
		}

		if ((colision.gameObject.CompareTag("Iluminati") == true))
		{
			RestaBarra(100);
			panel.SetActive(true);
			audioSource.PlayOneShot(risa);

			Invoke("Fin", 3);
		}



	}
    public void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.CompareTag("Objeto"))
        {
			RestaBarra(20);
			
		}
		if (currentHealth == 0)
		{
			//transform.position = spawnPoint.position;
			//Destruccion();
			Invoke("Fin", 3);
		}


	}

    void RestaBarra(int damage)
	{
		currentHealth -= damage;

		healthBar.setHealth(currentHealth);
	}

	/*
	public void Destruccion()
	{
		Destroy(this.gameObject);
		

		GestorDeAudio.instancia.ReproducirSonido("Muerte");
		SceneManager.LoadScene("GameOver");

		//Persistencia.instancia.GuardarDataPersistencia();

	}*/
	private void Fin()
	{
		//SceneManager.LoadScene("InterludeMenu");
		//SceneManager.LoadScene("LevelOne");
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
		transform.position = spawnPoint.position;
		panel.SetActive(false);

	}

	
	

}

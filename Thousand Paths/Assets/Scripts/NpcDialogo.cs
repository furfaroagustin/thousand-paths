using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NpcDialogo : MonoBehaviour
{
    public GameObject simboloMision;
    public FirstPersonMovement jugador;
    public GameObject panelNpc;
    public GameObject panelNpc2;
    public GameObject panelNpcMision;
    public TextMeshProUGUI textoMision;
    public bool jugadorCerca;
    public bool aceptarMision;
    public GameObject[] objetivos;
    public int numeroObjetivos;
    public GameObject botonMision;
    public GameObject puerta;

    private void Start()
    {
        numeroObjetivos = objetivos.Length;
        textoMision.text = "hello adventurer, I need to find the lost key, can you help me?";
        jugador = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonMovement>();
        simboloMision.SetActive(true);
        panelNpc.SetActive(false);
        puerta.SetActive(true);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && aceptarMision == false)
        {
            Vector3 posicionJugador = new Vector3(transform.position.x, jugador.gameObject.transform.position.y, transform.position.z);
            jugador.gameObject.transform.LookAt(posicionJugador);


            jugador.enabled = false;
            panelNpc.SetActive(false);
            panelNpc2.SetActive(true);
        }
     
       


    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            jugadorCerca = true;
            if (aceptarMision == false)
            {
                panelNpc.SetActive(true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            jugadorCerca = false;
            panelNpc.SetActive(false);
            panelNpc2.SetActive(false);

        }
    }
    public void no()
    {
        jugador.enabled = true;
        panelNpc2.SetActive(false);
        panelNpc.SetActive(true);

    }

    public void si()
    {
        jugador.enabled = true;
        aceptarMision = true;
        for (int i = 0; i < objetivos.Length; i++)
        {
            objetivos[i].SetActive(true);

        }
        jugadorCerca = false;
        simboloMision.SetActive(false);
        panelNpc.SetActive(false);
        panelNpc2.SetActive(false);
        panelNpcMision.SetActive(true);
        panelNpcMision.SetActive(false);
        puerta.SetActive(false);

    }
    public void nso()
    {
        panelNpcMision.SetActive(false);
        puerta.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public GameObject objeto;//bullet
    public Transform puntoSpawn;//spawnPoint

    public float fuerzaDisparo = 1500;//shotforce
    public float shotRate = 0.5f;//radio de disparo

    private float shotRateTime = 0;//tiempo entre disparo 

    void Update()
    {
        if (Input.GetKey("space"))
        {
            if (Time.time > shotRateTime)
            {
                GameObject nuevaBala;//NewBullet
                nuevaBala = Instantiate(objeto, puntoSpawn.position, puntoSpawn.rotation);
                nuevaBala.GetComponent<Rigidbody>().AddForce(puntoSpawn.forward * fuerzaDisparo);
                shotRateTime = Time.time + shotRate;
            }
        }
    }
}

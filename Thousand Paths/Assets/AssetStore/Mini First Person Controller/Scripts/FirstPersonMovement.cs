﻿using System.Collections.Generic;
using UnityEngine;

public class FirstPersonMovement : MonoBehaviour
{
    public float speed = 5;

    [Header("Running")]
    public bool canRun = true;
    public bool IsRunning { get; private set; }
    public float runSpeed = 9;
    public KeyCode runningKey = KeyCode.LeftShift;

    Rigidbody rigidbody;
    /// <summary> Functions to override movement speed. Will use the last added override. </summary>
    public List<System.Func<float>> speedOverrides = new List<System.Func<float>>();



    public GameObject objeto;//bullet
    public Transform puntoSpawn;//spawnPoint

    public float fuerzaDisparo = 1500;//shotforce
    public float shotRate = 0.5f;//radio de disparo
    private float shotRateTime = 0;//tiempo entre disparo 
    
    protected AudioSource audioSource;
    public AudioClip musicaObjeto;
    //flecha
    public GameObject flecha;


    //salto
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    public float jumpRate = 0.5f;
    private float jumpRateTime = 0;


    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        col = GetComponent<CapsuleCollider>();
        flecha.SetActive(false);
    }
    void Awake()
    {
        // Get the rigidbody on this.
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // Update IsRunning from input.
        IsRunning = canRun && Input.GetKey(runningKey);

        // Get targetMovingSpeed.
        float targetMovingSpeed = IsRunning ? runSpeed : speed;
        if (speedOverrides.Count > 0)
        {
            targetMovingSpeed = speedOverrides[speedOverrides.Count - 1]();
        }

        // Get targetVelocity from input.
        Vector2 targetVelocity =new Vector2( Input.GetAxis("Horizontal") * targetMovingSpeed, Input.GetAxis("Vertical") * targetMovingSpeed);

        // Apply movement.
        rigidbody.velocity = transform.rotation * new Vector3(targetVelocity.x, rigidbody.velocity.y, targetVelocity.y);
    }
    private void Update()
    {
        if (Input.GetKey("space"))
        {
            if (Time.time > shotRateTime)
            {
                audioSource.PlayOneShot(musicaObjeto);
                GameObject nuevaBala;//NewBullet
                nuevaBala = Instantiate(objeto, puntoSpawn.position, puntoSpawn.rotation);
                nuevaBala.GetComponent<Rigidbody>().AddForce(puntoSpawn.forward * fuerzaDisparo);
                shotRateTime = Time.time + shotRate;
            }
        }

        
        

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            Cursor.lockState = CursorLockMode.Locked;
            
        }
    }

}
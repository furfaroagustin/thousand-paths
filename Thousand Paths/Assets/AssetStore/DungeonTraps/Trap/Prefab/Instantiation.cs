using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiation : MonoBehaviour
{
    public float tiempoCreacion = 2f;
    public GameObject prefab;
    private int count = 0;
    void Start()
    {
        InvokeRepeating("Creando", 0.0F, tiempoCreacion);
    }



    void Creando()
    {
        GameObject cube = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
        cube.name = "Parth" + count++;
        Destroy(cube, 10f);
    }
}

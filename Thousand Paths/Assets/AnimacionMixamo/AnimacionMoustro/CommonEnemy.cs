using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class CommonEnemy : MonoBehaviour
{
    #region Singleton

    public static CommonEnemy instance;
    //sonido
    public AudioClip rugido;
    public AudioClip ataque;
    protected AudioSource audioSource;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    Animator anim;
    NavMeshAgent agent;


    public Transform player, target, canon;
    public GameObject bullet;
    public float gizmoRangeDetection, gizmoRangeAttack, shootForce;


    public int maxHP;
    private int currentHP;

    
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();

        player = GameObject.FindGameObjectWithTag("Player").transform;

        currentHP = maxHP;

        //sonido
        audioSource = GetComponent<AudioSource>();


        
        


    }

    void Update()
    {
        CalculateEnemyPos();
        DestroyEnemy();
       audioSource.PlayOneShot(rugido);
    }
    #region Movement
    private void CalculateEnemyPos()
    {
        gizmoRangeAttack = agent.stoppingDistance;
        float distance = Vector3.Distance(this.transform.position, player.position);

        if (distance < gizmoRangeDetection)
        {
            target = player;
            agent.stoppingDistance = 2f;
            agent.SetDestination(target.position);
            AutoLooker();

            if (distance < gizmoRangeAttack)
            {
                anim.SetBool("idle", false);
                anim.SetBool("walk", false);
                anim.SetBool("fire", true);
                AutoLooker();

                Debug.DrawLine(canon.position, target.position, Color.red);
            }
            else
            {
                anim.SetBool("idle", false);
                anim.SetBool("walk", true);
                anim.SetBool("fire", false);
            }
        }
        else
        {
            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("fire", false);
            target = null;
            agent.stoppingDistance = 0f;
        }
    }
    private void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }
    #endregion

    #region Attack
    public void Attack()
    {
        GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, canon.transform.rotation);
        Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
        rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
        Destroy(currentBullet, 1.5f);
    }
    #endregion

    #region Stats
    private void DestroyEnemy()
    {
        if (currentHP <= 0)
        {
            anim.SetBool("idle", false);
            anim.SetBool("walk", false);
            anim.SetBool("fire", false);
            //  anim.SetBool("dead", true);
            // anim.SetInteger("death", Random.Range(0, 3));

            // GameManagerScript.instance.currentKilledEnemies++;

            gameObject.GetComponent<CommonEnemy>().enabled = false;
        }
    }
    public void StopDeathAnim()
    {
        Destroy(this.gameObject);
    }
    #endregion
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRangeDetection);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            audioSource.PlayOneShot(ataque);
        }

    }

   
}
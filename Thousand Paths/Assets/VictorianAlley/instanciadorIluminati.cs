using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instanciadorIluminati : MonoBehaviour
{
    
    public GameObject prefab;
    private int count = 0;

    private Vector3 spawnPos;
    public float respawnSmallPlatf;
    void Start()
    {
       
    }
    private void Update()
    {
       SpawnSmallPlatf();
    }

    private void SpawnSmallPlatf()                 //Metodo para instanciar objetos en escena.
    {
        respawnSmallPlatf -= Time.deltaTime;

        spawnPos.x = 0f;
        spawnPos.y = Random.Range(0f, 0.5f);
        spawnPos.z = 0f;

        if (respawnSmallPlatf <= 0)
        {
            Instantiate(prefab, spawnPos, Quaternion.identity);
            respawnSmallPlatf = Random.Range(5, 10);
            Destroy(gameObject, 2f);
        }
    }



}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodigoInstanciador : MonoBehaviour
{
    public GameObject triangulo;
  
    private void Start()
    {
 
        triangulo.SetActive(false);
       
    }
   /* private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") == true)
        {

           triangulo.SetActive(true);
        }



    }*/
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) 
        {
            
            triangulo.SetActive(true);


            Invoke("Desactivado", 8);


        }
    }
    private void Desactivado()
    {
        triangulo.SetActive(false);
    }
}